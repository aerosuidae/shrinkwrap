data:extend(
{
  {
    type = "item",
    name = "shrink-wrap",
    icon = "__shrinkwrap__/graphics/icons/shrink-wrap.png",
    icon_size = 32,
    flags = {"goes-to-main-inventory"},
    subgroup = "intermediate-product",
    order = "d[shrink-wrap]",
    stack_size = 100,
    auto_pallet = false
  }
})

