data:extend(
{
  {
    type = "recipe",
    name = "shrink-wrap",
    category = "crafting",
    energy_required = 1,
    subgroup = "intermediate-product",
    enabled = false,
    ingredients =
    {
      {type="item", name="plastic-bar", amount=1},
    },
    result = "shrink-wrap",
    result_count = 10,
    auto_shrink_wrap = false,
  }
})
