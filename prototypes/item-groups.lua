data:extend(
{
  {
    type = "item-group",
    name = "shrink-wrap",
    order = "ca",
    icon = "__shrinkwrap__/graphics/technology/shrink-wrapping.png",
    icon_size = 128,
  },
  {
    type = "item-subgroup",
    name = "shrink-wrap-apply",
    group = "shrink-wrap",
    order = "a"
  },
  {
    type = "item-subgroup",
    name = "shrink-wrap-remove",
    group = "shrink-wrap",
    order = "b"
  }
})
