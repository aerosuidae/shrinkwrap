data:extend(
{
  {
    type = "technology",
    name = "shrink-wrapping",
    icon = "__shrinkwrap__/graphics/technology/shrink-wrapping.png",
    icon_size = 128,
    effects =
    {
      {
        type = "unlock-recipe",
        recipe = "shrink-wrap"
      }
    },
    prerequisites = {"plastics"},
    unit =
    {
      count = 150,
      ingredients =
      {
        {"science-pack-1", 1},
        {"science-pack-2", 1}
      },
      time = 30
    },
    order = "c-o-a",
  }
})
