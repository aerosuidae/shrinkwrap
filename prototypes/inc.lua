-- This auto-generates shrink wrapped items and fill/empty recipes for every item
-- Based on auto barreling from __base__/data-updates.lua
-- Based on https://mods.factorio.com/mod/pallets

local shrink_wrapped_stack = 5

local icon_shrink_wrapped = "__shrinkwrap__/graphics/icons/shrink-wrapped.png"
local icon_shrink_wrap = "__shrinkwrap__/graphics/icons/shrink-wrap.png"
local icon_shrink_unwrap = "__shrinkwrap__/graphics/icons/shrink-unwrap.png"

local allow_barrels = false
local allow_entities = false
local allow_equipment = false

local function get_item(name)
  local items = data.raw["item"]
  if items then
    return items[name]
  end
  return nil
end

local function get_technology(name)
  local technologies = data.raw["technology"]
  if technologies then
    return technologies[name]
  end
  return nil
end

local function contains(table,element)
  if table then
    for _, value in pairs(table) do
      if value == element then
        return true
      end
    end
  end
  return false
end

local function item_icon(item)
  return {
    {
      icon = icon_shrink_wrapped
    },
    {
      icon = item.icon,
      scale = 0.8
    }
  }
end

local function apply_icon(item)
  return item_icon(item)
end

local function remove_icon(item)
  return {
    {
      icon = icon_shrink_wrapped
    },
    {
      icon = item.icon,
      scale = 0.8
    },
    {
      icon = icon_shrink_unwrap,
      scale = 0.5,
      shift = {8, 8}
    },
  }
end


local function item_name(item)
  -- For items, we should look in item-name.x
  -- For entities, look at entity-name.x
  if item.localised_name then
    return item.localised_name
  elseif item.place_result then
    return {"entity-name." .. item.name}
  elseif item.placed_as_equipment_result then
    return {"equipment-name." .. item.name}
  else
    return {"item-name." .. item.name}
  end
end

local function batch_size(item)
  local n = item.stack_size / shrink_wrapped_stack
  if n < 1 then
    n = 1
  end
  return n
end

local function wrapped_item(item)
  local result =
  {
    type = "item",
    name = "shrink-wrapped-" .. item.name,
    localised_name = {"item-name.shrink-wrapped", item_name(item)},
    icons = item_icon(item),
    icon_size = 32,
    flags = {"goes-to-main-inventory"},
    subgroup = "shrink-wrap-apply",
    order = "b[shrink-wrapped-" .. item.name .."]",
    stack_size = shrink_wrapped_stack
  }

  if contains(item.flags,"hidden") then
    table.insert(result.flags, "hidden")
  end

  data:extend({result})
  return result
end

local function wrap_item(item)
  local existing_item = get_item("shrink-wrapped-" .. item.name)
  if existing_item then
    return existing_item
  end
  return wrapped_item(item)
end

local function apply_recipe(item)
  local recipe = {
    type = "recipe",
    name = "shrink-wrap-apply-" .. item.name,
    localised_name = {"recipe-name.shrink-wrap-apply", item_name(item)},
    category = "advanced-crafting",
    energy_required = 0.2,
    subgroup = "shrink-wrap-apply",
    order = "b[shrink-wrap-apply-" .. item.subgroup .. "-" .. item.name .."]",
    enabled = false,
    icons = apply_icon(item),
    icon_size = 32,
    ingredients = {
      {type = "item", name = item.name, amount = batch_size(item)},
      {type = "item", name = "shrink-wrap", amount = 1},
    },
    results = {
      {type = "item", name = "shrink-wrapped-" .. item.name, amount = 1}
    },
    hide_from_stats = true,
    allow_decomposition = false
  }

  data:extend({recipe})
  return recipe
end

local function remove_recipe(item)
  local recipe = {
    type = "recipe",
    name = "shrink-wrap-remove-" .. item.name,
    localised_name = {"recipe-name.shrink-wrap-remove", item_name(item)},
    category = "advanced-crafting",
    energy_required = 0.2,
    subgroup = "shrink-wrap-remove",
    order = "c[shrink-wrap-remove-" .. item.subgroup .. "-" .. item.name .."]",
    enabled = false,
    icons = remove_icon(item),
    icon_size = 32,
    ingredients = {
      {type = "item", name = "shrink-wrapped-" .. item.name, amount = 1}
    },
    results = {
      {type = "item", name = item.name, amount = batch_size(item)},
    },
    hide_from_stats = true,
    allow_decomposition = false
  }

  data:extend({recipe})
  return recipe
end

local function recipes(item)
  local recipes = data.raw["recipe"]
  if recipes then
    return recipes["shrink-wrap-apply-" .. item.name], recipes["shrink-wrap-remove-" .. item.name]
  end
  return nil
end

local function create_recipe(item)
  local load_recipe, unload_recipe = recipes(item)

  if not load_recipe then
    load_recipe = apply_recipe(item)
  end
  if not unload_recipe then
    unload_recipe = remove_recipe(item)
  end

  return load_recipe, unload_recipe
end

local function attach_tech(load_recipe, unload_recipe, technology)
  local unlock_key = "unlock-recipe"
  local effects = technology.effects

  if not effects then
    technology.effects = {}
    effects = technology.effects
  end

  local add_load_recipe = true
  local add_unload_recipe = true

  for k,v in pairs(effects) do
    if k == unlock_key then
      local recipe = v.recipe
      if recipe == load_recipe.name then
        add_load_recipe = false
      elseif recipe == unload_recipe.name then
        add_unload_recipe = false
      end
    end
  end

  if add_load_recipe then
    table.insert(effects, {type = unlock_key, recipe = load_recipe.name})
  end
  if add_unload_recipe then
    table.insert(effects, {type = unlock_key, recipe = unload_recipe.name})
  end
end

local subgroups = {
  ["ammo"] = true,
  ["intermediate-product"] = true,
  ["module"] = true,
  ["raw-material"] = true,
  ["raw-resource"] = true,
  ["science-pack"] = true,
  ["terrain"] = true,
}

local specials = {
  ["pipe"] = true,
}

local function able(item)
  if item.subgroup == "shrink-wrap-apply" then
    return false
  end
  if item.name == "shrink-wrap" then
    return false
  end
  if batch_size(item) == 1 then
    return false
  end
  if subgroups[item.subgroup] ~= nil or specials[item.name] ~= nil then
    return true
  end
--  local proto = find_proto(item)
--  if proto ~= nil then
--    local bbox = proto.selection_box
--    local dx = bbox[2][1] - bbox[1][1]
--    local dy = bbox[2][2] - bbox[1][2]
--    local area = math.abs(dx*dy)
--    if area < 2 then
--      return true
--    end
--  end
  return false
end

local function allow_shrink_wrap(item)
  if contains(item.flags,"hidden") then
    return false
  elseif (item.auto_shrink_wrap == nil or item.auto_shrink_wrap) and item.icon and able(item) then
    if specials[item.name] then
      return true
    elseif item.subgroup == "fill-barrel" or item.name == "empty-barrel" then
      return allow_barrels
    elseif item.place_result then
      return allow_entities
    elseif item.placed_as_equipment_result then
      return allow_equipment
    else
      return true
    end
  end
  return false
end

local function process_items(items)
  local tech = get_technology("shrink-wrapping")

  if not items then
    log("Auto shrink wrap generation is disabled.")
  end

  for name,item in pairs(items) do
    if allow_shrink_wrap(item) then
      wrap_item(item)

      -- check if the item has a shrink wrapping recipe if not - create one
      local load_recipe, unload_recipe = create_recipe(item)

      -- check if the loading/unloading recipe exists in the unlock list of the technology if not - add it
      attach_tech(load_recipe, unload_recipe, tech)
    end
  end
end

local function setup()
  process_items(data.raw["item"], tech)
  process_items(data.raw["module"], tech)
  process_items(data.raw["tool"], tech)
end

return {
  setup = setup,
}
