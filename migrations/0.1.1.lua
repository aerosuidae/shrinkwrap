for i, force in pairs(game.forces) do
  force.reset_recipes()
  force.reset_technologies()

  if force.technologies["shrink-wrapping"].researched then
    force.technologies["shrink-wrapping"].researched = false
    force.technologies["shrink-wrapping"].researched = true
  end
end