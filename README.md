# Factorio Mod: Shrink Wrap

Shrink wrap packs of items.

* "Shrink wrapping" research is available (red+green) after Plastics
* Pack size is 1/5 the stack size; ie. 1x200 green circuit stack == 5x40 green circuit packs
* Wrapping and unwrapping is done via assembler machine
* Shrink wrap cost is relatively low: 1x Plastic bar gives 10x Shrink wrap
* Only certain item sub-groups may be shrink wrapped:
    * ammo
    * intermediate products
    * raw materials and resources
    * science packs
    * terrain
* Also, any item in those groups that has a stack size <= 5 (where pack size would be 1) is ignored
* Other mods that add items to those groups should automatically be included

## Thanks

Design and code adapted from:

* https://mods.factorio.com/mod/pallets
* https://github.com/twanvl/pallets-factorio-mod